---
title: Calendar calculator
subtitle: AlienOne Security
author:
- Nick Jensen \<nick@alienonesecurity.com\>
date: '2019-03-19'
abstract: |
  Calendar calculator
header: Calendar calculator
footer: AlienOne Security
toc: true
numbersections: true
colorlinks: true
links-as-notes: true
classoption: twoside
papersize: letter
geometry: margin=1.0in
#papersize: A4
#geometry: margin=2.5cm
---

\pagebreak

\listoffigures

\pagebreak

\listoftables

\pagebreak

\listoflistings

\pagebreak

# Summary

[calcalc](https://gitlab.com/njen/calcalc) is a calendar calculator.

![Project avatar](fig/avatar.jpg){width=25%}

\pagebreak

# Installation

Install calcalc via one of the following methods:

* From bdist wheel: `pip install dist/calcalc-0.2.0-py3-none-any.whl`
* From sdist: `pip install dist/calcalc-0.2.0.tar.gz`

\pagebreak

# Usage

The `calcalc.py` script can be run directly (e.g. `./calcalc.py`) or via the
`calcalc` shim if installed.

```{.text caption="usage"}
$ calcalc -h
usage: calcalc [-h] {test,bizdays} ...

Calendar calculator

positional arguments:
  {test,bizdays}
    test          run doctest tests
    bizdays       Business days calculator

optional arguments:
  -h, --help      show this help message and exit
```

## Subcommands

### `test`

Run doctest tests

```{.text caption="test usage"}
$ ./calcalc.py test -h
usage: calcalc test [-h]

run doctest tests

optional arguments:
  -h, --help  show this help message and exit
```

\pagebreak

#### `test` example

```{.text caption="test example"}
$ ./calcalc.py test
Trying:
    a = cli(['bizdays', '1,2019-04-01'])
Expecting:
    * start (Mon 2019-04-01) + 1 business days
        * first: Mon 2019-04-01
        * last: Mon 2019-04-01
        * 1 calendar days
        * 1 business days
        * 0 weekend days
        * 0 holidays
    <BLANKLINE>
ok
Trying:
    a = cli(['bizdays', '90,2019-03-31'])
Expecting:
    * start (Sun 2019-03-31) + 90 business days
        * first: Mon 2019-04-01
        * last: Tue 2019-08-06
        * 129 calendar days
        * 90 business days
        * 37 weekend days
        * 2 holidays
            * Mon 2019-05-27 Memorial Day
            * Thu 2019-07-04 Independence Day
    <BLANKLINE>
ok
Trying:
    a = cli(['bizdays', '90,2019-04-01'])
Expecting:
    * start (Mon 2019-04-01) + 90 business days
        * first: Mon 2019-04-01
        * last: Tue 2019-08-06
        * 128 calendar days
        * 90 business days
        * 36 weekend days
        * 2 holidays
            * Mon 2019-05-27 Memorial Day
            * Thu 2019-07-04 Independence Day
    <BLANKLINE>
ok
Trying:
    a = cli(['bizdays', '270,2019-04-01'])
Expecting:
    * start (Mon 2019-04-01) + 270 business days
        * first: Mon 2019-04-01
        * last: Fri 2020-04-24
        * 390 calendar days
        * 270 business days
        * 110 weekend days
        * 10 holidays
            * Mon 2019-05-27 Memorial Day
            * Thu 2019-07-04 Independence Day
            * Mon 2019-09-02 Labor Day
            * Mon 2019-10-14 Columbus Day
            * Mon 2019-11-11 Veterans Day
            * Thu 2019-11-28 Thanksgiving Day
            * Wed 2019-12-25 Christmas Day
            * Wed 2020-01-01 New Year's Day
            * Mon 2020-01-20 Martin Luther King Jr Day
            * Mon 2020-02-17 Washington's Birthday
    <BLANKLINE>
ok
Trying:
    a = cli(['bizdays', '180,2019-08-05'])
Expecting:
    * start (Mon 2019-08-05) + 180 business days
        * first: Mon 2019-08-05
        * last: Wed 2020-04-22
        * 262 calendar days
        * 180 business days
        * 74 weekend days
        * 8 holidays
            * Mon 2019-09-02 Labor Day
            * Mon 2019-10-14 Columbus Day
            * Mon 2019-11-11 Veterans Day
            * Thu 2019-11-28 Thanksgiving Day
            * Wed 2019-12-25 Christmas Day
            * Wed 2020-01-01 New Year's Day
            * Mon 2020-01-20 Martin Luther King Jr Day
            * Mon 2020-02-17 Washington's Birthday
    <BLANKLINE>
ok
10 items had no tests:
    __main__
    __main__.bizdays
    __main__.bizdays.__init__
    __main__.cli
    __main__.default
    __main__.subcommand
    __main__.subcommand.__init__
    __main__.subcommand.func
    __main__.test
    __main__.test.func
1 items passed all tests:
   5 tests in __main__.bizdays.func
5 tests in 11 items.
5 passed and 0 failed.
Test passed.
```

\pagebreak

### `bizdays`

Business days calculator

```{.text caption="bizdays usage"}
$ calcalc bizdays -h
usage: calcalc bizdays [-h] STR [STR ...]

Business days calculator

positional arguments:
  STR         comma-separated days and optional start date

optional arguments:
  -h, --help  show this help message and exit
```

