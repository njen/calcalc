#!/usr/bin/env python


"""
Setuptools script
"""


# # Metadata


__status__ = 'Development'
__date__ = '2019-03-15'
__version__ = '0.2.0'
__author__ = 'Nick Jensen'
__email__ = 'nick@alienonesecurity.com'
__copyright__ = 'Copyright 2019, Nick Jensen'
__license__ = 'CC BY-NC-ND 4.0',
__credits__ = (
    'Nick Jensen',
)
__maintainer__ = __credits__[-1]


# # Variables


cfg = dict(
    name='calcalc',
    description='Calendar calculator',
    url='https://gitlab.com/njen/calcalc',
    py_modules=['calcalc'],
    #packages=[...],
    #packages=setuptools.find_packages(),
    install_requires=[],
    classifiers=[ # https://pypi.org/classifiers/
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: CC BY-NC-ND 4.0',
        'Topic :: Utilities',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Programming Language :: Python :: 3.7',
    ],
    long_description=open('README.md').read(),
    version=__version__,
    author=__author__,
    author_email=__email__,
    entry_points={
        'console_scripts': [
            'calcalc = calcalc:cli',
        ],
    },
)


# # Main


if __name__ == '__main__':
    from setuptools import setup
    setup(**cfg)


