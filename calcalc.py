#!/usr/bin/env python


"""
Calendar calculator
"""


# # Metadata


__status__ = 'Development'
__date__ = '2019-03-15'
__version__ = '0.2.0'
__author__ = 'Nick Jensen'
__email__ = 'nick@alienonesecurity.com'
__copyright__ = 'Copyright 2019, Nick Jensen'
__license__ = 'CC BY-NC-ND 4.0',
__credits__ = (
    'Nick Jensen',
)
__maintainer__ = __credits__[-1]


# # Imports


import argparse
import doctest
import re

from datetime import date
from datetime import datetime
from datetime import timedelta


# # Classes


class default:
    date_fmt = '%Y-%m-%d'
    disp_fmt = '%a %Y-%m-%d'


class subcommand:

    def __init__(self, sub_parser, root_parser):
        """
        name subcommand
        :param sub_parser: argpase._SubParsersAction
        :param root_parser: argparse.ArgumentParser
        """
        self.sub_parser = sub_parser
        self.root_parser = root_parser
        desc = self.func.__doc__.strip().split('\n')[0]
        self.parser = sub_parser.add_parser(
            self.__class__.__name__,
            description=desc,
            help=desc,
        )
        self.parser.set_defaults(func=self.func)

        # Add common arguments for ALL subcommands here
        #self.parser.add_argument(
        #    'paths',
        #    metavar='PATH',
        #    action='store',
        #    nargs='+',
        #    help='file or directory path(s)',
        #)

    def func(self, a):
        """
        some command
        :param a: Iterable
        """
        pass


class test(subcommand):

    def func(self, a):
        """
        run doctest tests
        :param a: Iterable
        """
        doctest.testmod(verbose=True)


class bizdays(subcommand):

    def __init__(self, sub_parser, root_parser):
        """
        bizdays subcommand
        :param sub_parser: argpase._SubParsersAction
        :param root_parser: argparse.ArgumentParser
        """
        super().__init__(sub_parser, root_parser)

        self.parser.add_argument(
            'args',
            metavar='STR',
            action='store',
            nargs='+',
            help='comma-separated days and optional start date',
        )


    @staticmethod
    def func(a):
        """
        Business days calculator

        >>> a = cli(['bizdays', '1,2019-04-01'])
        * start (Mon 2019-04-01) + 1 business days
            * first: Mon 2019-04-01
            * last: Mon 2019-04-01
            * 1 calendar days
            * 1 business days
            * 0 weekend days
            * 0 holidays
        <BLANKLINE>
        >>> a = cli(['bizdays', '90,2019-03-31'])
        * start (Sun 2019-03-31) + 90 business days
            * first: Mon 2019-04-01
            * last: Tue 2019-08-06
            * 129 calendar days
            * 90 business days
            * 37 weekend days
            * 2 holidays
                * Mon 2019-05-27 Memorial Day
                * Thu 2019-07-04 Independence Day
        <BLANKLINE>
        >>> a = cli(['bizdays', '90,2019-04-01'])
        * start (Mon 2019-04-01) + 90 business days
            * first: Mon 2019-04-01
            * last: Tue 2019-08-06
            * 128 calendar days
            * 90 business days
            * 36 weekend days
            * 2 holidays
                * Mon 2019-05-27 Memorial Day
                * Thu 2019-07-04 Independence Day
        <BLANKLINE>
        >>> a = cli(['bizdays', '270,2019-04-01'])
        * start (Mon 2019-04-01) + 270 business days
            * first: Mon 2019-04-01
            * last: Fri 2020-04-24
            * 390 calendar days
            * 270 business days
            * 110 weekend days
            * 10 holidays
                * Mon 2019-05-27 Memorial Day
                * Thu 2019-07-04 Independence Day
                * Mon 2019-09-02 Labor Day
                * Mon 2019-10-14 Columbus Day
                * Mon 2019-11-11 Veterans Day
                * Thu 2019-11-28 Thanksgiving Day
                * Wed 2019-12-25 Christmas Day
                * Wed 2020-01-01 New Year's Day
                * Mon 2020-01-20 Martin Luther King Jr Day
                * Mon 2020-02-17 Washington's Birthday
        <BLANKLINE>
        >>> a = cli(['bizdays', '180,2019-08-05'])
        * start (Mon 2019-08-05) + 180 business days
            * first: Mon 2019-08-05
            * last: Wed 2020-04-22
            * 262 calendar days
            * 180 business days
            * 74 weekend days
            * 8 holidays
                * Mon 2019-09-02 Labor Day
                * Mon 2019-10-14 Columbus Day
                * Mon 2019-11-11 Veterans Day
                * Thu 2019-11-28 Thanksgiving Day
                * Wed 2019-12-25 Christmas Day
                * Wed 2020-01-01 New Year's Day
                * Mon 2020-01-20 Martin Luther King Jr Day
                * Mon 2020-02-17 Washington's Birthday
        <BLANKLINE>

        :param a: Iterable
        """

        # constants
        weekend = (5, 6)
        holidays = {
            '2019-01-01': 'New Year\'s Day',
            '2019-01-21': 'Birthday of Martin Luther King, Jr.',
            '2019-02-18': 'Washington\'s Birthday',
            '2019-05-27': 'Memorial Day',
            '2019-07-04': 'Independence Day',
            '2019-09-02': 'Labor Day',
            '2019-10-14': 'Columbus Day',
            '2019-11-11': 'Veterans Day',
            '2019-11-28': 'Thanksgiving Day',
            '2019-12-25': 'Christmas Day',

            '2020-01-01': 'New Year\'s Day',
            '2020-01-20': 'Martin Luther King Jr Day',
            '2020-02-17': 'Washington\'s Birthday',
            '2020-05-25': 'Memorial Day',
            '2020-07-03': 'Independence Day',
            '2020-09-07': 'Labor Day',
            '2020-10-12': 'Columbus Day',
            '2020-11-11': 'Veterans Day',
            '2020-11-26': 'Thanksgiving Day',
            '2020-12-25': 'Christmas Day',
        }
        one_day = timedelta(days=1)

        # each days[,start] arg
        for arg in a.args:

            if re.search(',', arg):
                # days,start arg
                days, start = arg.split(',')
                t = datetime.strptime(start, default.date_fmt)
                start = t.date()
                first = t.date()
                last = t.date()
            else:
                # days arg
                days = arg
                start = date.today()
                first = date.today()
                last = date.today()

            # state variables
            d = int(days)
            cal_days = 0
            weekend_days = 0
            holidays_ = set()

            # find first business day
            startf = start.strftime(default.date_fmt)
            if start.weekday() in weekend:
                d += 1
                first += one_day
                weekend_days += 1
            elif startf in holidays:
                d += 1
                first += one_day
                holidays_.add(start)

            # find last business day
            while d > 0:
                last += one_day
                lastf = last.strftime(default.date_fmt)
                cal_days += 1
                if last.weekday() in weekend:
                    weekend_days += 1
                    d += 1
                elif lastf in holidays:
                    holidays_.add(last)
                    d += 1
                d -= 1
            last -= one_day
            while last.weekday() in weekend:
                last -= one_day
                cal_days -= 1
                weekend_days -= 1

            # report
            startf = start.strftime(default.disp_fmt)
            firstf = first.strftime(default.disp_fmt)
            lastf = last.strftime(default.disp_fmt)
            print(f'* start ({startf}) + {days} business days')
            print(f'    * first: {firstf}')
            print(f'    * last: {lastf}')
            print(f'    * {cal_days} calendar days')
            print(f'    * {days} business days')
            print(f'    * {weekend_days} weekend days')
            print(f'    * {len(holidays_)} holidays')
            if len(holidays_) > 0:
                for day in sorted(holidays_):
                    dayf = day.strftime(default.disp_fmt)
                    desc = holidays[day.strftime(default.date_fmt)]
                    print(f'        * {dayf} {desc}')
            print()


# # Functions


def cli(argv=None):
    """
    Command line interface
    :param argv: Iterable
    """
    root_parser = argparse.ArgumentParser(description=__doc__)

    # Add subcommands
    subcommand_classes = (
        test,
        bizdays,
    )
    sub_parser = root_parser.add_subparsers(dest='subcommand')
    subcommands = [cls(sub_parser, root_parser) for cls in subcommand_classes]

    # Parse the arguments
    args = root_parser.parse_args(argv)

    # Print usage and exit if no subcommand was provided
    if not args.subcommand:
        root_parser.print_help()
        exit(0)

    # Run the subcommand
    args.func(args)


# # Main


if __name__ == '__main__':
    cli()


